#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

#
# # Authors informations
#
# @author: HUC Stéphane
# @email: <devs@stephane-huc.net>
# @url: http://stephane-huc.net
#
# @license : OpenBSD
#

''' Launch YUP.py '''

# modules needed
import logging
import logging.config
import logging.handlers
import os
import platform
import sys
import time

# # i18n (internationalisation)
try:
    import gettext
    gettext.bindtextdomain('YUP', 'locale')
    gettext.textdomain('YUP')
    _ = gettext.gettext
except ImportError:
    print(f'Cant import gettext')
    sys.exit(1)

# Import personals modules
try:
    from modules.config import Config as Cfg
except ImportError:
    print(f'Cant import config module onto YUP launcher')
    sys.exit(1)
     
try:
    from modules.gui import Gui
except ImportError:
    print(f'Cant import Gui module onto YUP launcher')
    sys.exit(1) 

try:
    from modules.tools import Tools as tool
except ImportError:
    print(f'Cant import Tools module onto YUP launcher')
    sys.exit(1) 

# clear 
os.system("clear")

# globals
DESCR = 'Yet another Uploader Pixxie'
NAME = u'YUP'

class App(object):
    '''Application'''

    def __init__(self):

        ###
        # initialize general variables
        ###
        self.dir = os.path.dirname(os.path.abspath(sys.argv[0])) # os.getcwd()

        self.dirs = ['conf', 'log', 'modules']

        self.year = time.strftime('%Y', time.localtime())

        ###
        # initialize specific variables
        ###
        self.actions = {
            'default': 'toilelibre',

            'index': [
                'debianfr', 'debianfacile',
                'framapic', 
                'jirafeau', 
                'lutim', 
                'toilelibre'
                ],
            
            'debianfacile': {   # as Jyraphe
                'active': False,
                'days': {   # select_time
                    'default': 'none',
                    'index': ['none', 'minute', 'hour', 'day',
                                    'week', 'month', 'year'],
                    'day': {'active': True, 'label': _('Un jour'), },
                    'hour': {'active': True, 'label': _('Une heure'), },
                    'minute': {'active': True, 'label': _(u'Une minute'), },
                    'month': {'active': True, 'label': _(u'Un mois'), },
                    'none': {'active': True, 'label': _(u'Illimité'), },
                    'week': {'active': True, 'label': _(u'Une semaine'), },
                    'year': {'active': False, 'label': _(u'Une année'), },
                },
                'func': _(u'jirafeau'),
                'label': _(u'Debian Facile'),
                'max_file_size': 8388608,   # 8Mo
                'member': False,
                'urls': {
                    'upload': _(u'https://debian-facile.org/images/'),
                    'web': _(u'https://debian-facile.org/images/'),
                },
            },

            'debianfr': {
                'active': False,
                'func': _(u'toilelibre'),
                'label': _(u'Debian-fr'),
                'max_file_size': 5632000,   # 5Mo
                'member': True,
                'urls': {
                    'member': _(u'https://pix.debian-fr.xyz/?action=login'),
                    'upload': _(u'https://pix.debian-fr.xyz/?action=upload'),
                    },
                },

            'framapic': {
                'active': True,
                'days': {
                    'default': 180,
                    'index': [0, 1, 7, 30, 90, 180, 365],
                    0: {'active': False, 'label': _(u'Illimité'), },
                    1: {'active': True, 'label': _(u'Un jour'), },
                    7: {'active': True, 'label': _(u'Une semaine'), },
                    30: {'active': True, 'label': _(u'Un mois'), },
                    90: {'active': True, 'label': _(u'Trois mois'), },
                    180: {'active': True, 'label': _(u'Six mois'), },
                    365: {'active': True, 'label': _(u'Un an'), },
                    },
                'func': _(u'lutim'),
                'label': _(u'Framapic'),
                'member': False,
                'urls': {
                    'upload': _(u'https://framapic.org'),
                    },
                },
                
            'jirafeau': {
                'active': True,
                'days': {   # select_time
                    'default': 'month',
                    'index': ['none', 'minute', 'hour', 'day',
                                    'week', 'month', 'year'],
                    'day': {'active': True, 'label': _('Un jour'), },
                    'hour': {'active': True, 'label': _('Une heure'), },
                    'minute': {'active': True, 'label': _(u'Une minute'), },
                    'month': {'active': True, 'label': _(u'Un mois'), },
                    'none': {'active': False, 'label': _(u'Illimité'), },
                    'week': {'active': True, 'label': _(u'Une semaine'), },
                    'year': {'active': False, 'label': _(u'Une année'), },
                },
                'input_key': '',    # to type password
                'label': _(u'Jirafeau'),
                'max_file_size': 104857600,     # 100 Mo
                'member': False,
                'one_time_dl': 0,
                'urls': {
                    'upload': _(u'https://jirafeau.net/script.php'),
                    'web': _(u'https://jirafeau.net'),
                },
            },

            'lutim': {
                'active': False,
                'crypt': 1,
                'days': {
                    'default': 7,
                    'index': [0, 1, 7, 30, 90, 180, 365],
                    0: {'active': False, 'label': _(u'Illimité'), },
                    1: {'active': True, 'label': _(u'Un jour'), },
                    7: {'active': True, 'label': _(u'Une semaine'), },
                    30: {'active': True, 'label': _(u'Un mois'), },
                    90: {'active': True, 'label': _(u'Trois mois'), },
                    180: {'active': True, 'label': _(u'Six mois'), },
                    365: {'active': True, 'label': _(u'Un an'), },
                    },
                'first_view': 0,
                'format_output': {
                    'default': 'json',
                    'index': ['json', 'web'],
                    'json': {'active': True, 'label': _(u'JSON'), },
                    'web': {'active': True, 'label': _(u'HTML'), },
                },
                'keep_exif': 0,
                'label': _(u'Lutim'),
                'member': False,
                'retention': 0,
                'urls': {
                    'upload': _(u'https://lut.im'),
                    },
                },

            'toilelibre': {
                'active': True,
                'description': '',
                'img_infos': True,
                'label': _(u'Toile Libre'),
                'max_file_size': 15360000,  # 15 Mo
                'member': True,
                'private': 0,
                'tags': '',
                'urls': {
                    'member': _(u'https://pix.toile-libre.org/?action=login'),
                    'upload': _(u'https://pix.toile-libre.org/?action=upload'),
                    },
                },
        }

        self.authors = [u'Stéphane HUC']

        self.config = {
            'boolean': {
                #'default': 0,
                'index': [0, 1],
                0: {'active': True, 'label': 'Faux'},
                1: {'active': True, 'label': 'Vrai'},
            },

            'info': {
                'Main': {
                    'debug': False,
                    'theme': 'default',
                },
                'Services': {
                    'default': self.actions['default'],
                    'journal': True,
                    'logname': os.path.join(
                        os.path.expanduser("~"), '.config', NAME,
                        NAME + '.story.html'),
                    'mini_width': 200,
                },
                'lutim': {
                    'format_output':
                        self.actions['lutim']['format_output']['default'],
                },
                'toilelibre': {
                    'img_infos': self.actions['toilelibre']['img_infos'],
                }

            },

            'lang': 'fr',
        }
        
        self.files = {
            'config': os.path.join(
                os.path.expanduser("~"), '.config', 'YUP', 'yup.py.ini'),

            #'help': os.path.join(self.dir, 'conf', 'Help.html'),
            'html': os.path.join('/tmp', NAME + '.html'),
	    
            'images': {
                'icon': os.path.join(self.dir, 'img', 'Logo.xbm'),
                'logo': os.path.join(self.dir, 'img', 'Logo.png'),
            },

            'license': os.path.join(self.dir, 'LICENSE'),
            'log': os.path.join(self.dir, self.dirs[0], 'log.conf'),

            'src': {
                'bootstrap': { 
                    'css': os.path.join(
                        self.dir, self.dirs[0], 'bootstrap.min.css'),
                    'js': os.path.join(
                        self.dir, self.dirs[0], 'bootstrap.min.js'),
                },
                'config': os.path.join(self.dir, self.dirs[0], 'yup.py.ini'),
                'historic': os.path.join(
                    self.dir, self.dirs[0], NAME + '.story.html'),
                'html': os.path.join(
                    self.dir, self.dirs[0], NAME + '.html'),
                'personal': os.path.join(
                    os.path.expanduser("~"), '.config', 'YUP'),
            },

            'ui':  os.path.join(self.dir, 'test.ui'),

            'version': os.path.join(self.dir, 'VERSION'),
        }

        self.texts = {
            'about': {
                'comments': _(
                    u"Téléverser des images vers des services d'hébergement "
                    + u'pour publier dans les forums ...'
                ),
                'copyright': u" © 2015-" + self.year,
                'credits': [
                    self.authors[0] + ' <devs@stephane-huc.net>',
                    u"D'après une idée originale de : ",
                    u'Sebastien Charpentier "Cracolinux" ' + \
                    u'<cracolinux@mailoo.org>'
                ],
                'historic': _(u'Historique de vos images téléversées'),
                'license': _(u'License : <a href="' + self.files['license'] + \
                    '">OpenBSD</a>'),
            },
            
            'bool': {
                #'default': 0,
                #'index': [0, 1],
                0: {'active': True, 'label': 'Non'},
                1: {'active': True, 'label': 'Oui'},
            },

            'error': {
                'big_size':
                    _(u"L'image est trop grande '%s' ! \n ")
                    + _("La taille max autorisée est : %s"),
                'build_urls':
                    _(u'Il semble y avoir un problème pour construire ')
                    + _(u'les URLS nécessaires'),
                'connection':
                    _(u'Une erreur de connexion avec le serveur est arrivée !'),
                'http':
                    _(u'Une erreur HTTP avec le serveur est arrivée !'),
                'read_file':
                    _(u'Il semble y avoir un problème pour lire ')
                    + _(u'le fichier %s'),
                'redirect':
                    _(u'Trop de redirections sont essayées !'),
                'req_not_build': 
                    _(u"La requête ne semble pas s'être construite !"),
                'size':
                    _(u'Le poids du fichier est à 0 ...'),
                'timeout':
                    _(u'La requête a expirée !'),
                'timeout_connect':
                    _(u'La requête a expirée durant la connexion au serveur !'),
                'timeout_read':
                    _(u"Le serveur n'a pas répondu durant le temps imparti !"),
                'url':
                    _(u'Veuillez fournir une URL de connexion valide !'),
                'write_file':
                    _(u'Il semble y avoir un problème pour écrire ')
                    + _(u'le fichier %s'),
            },

            'filter': {
                'img': _(u'Fichiers images'),
            },

            'label': {
                'file_chooser': _(u'Parcourir'),
                # labels menu fichiers
                'menu_display': _(u'Affichage'),
                'menu_file': _(u'Fichier'),
                'menu_help': _(u'Aide'),
                'menu_quit': _(u'Quitter'),
                'menu_save': _(u'Enregistrer'),
                # labels menu affichage
                'menu_history': _(u'Historique'),
                'menu_config': _(u'Configuration'),
                # labels menu aide
                'menu_git': _(u'Github'),
                'menu_web': _(u'Site Web'),
                'menu_about': _(u'À-propos'),

                # label frame
                'frm_conn': _(u'Connexion'),
                'frm_info': _(u'Informations'),
                'frm_jrf': _(u'Services :: Jirafeau'),
                'frm_lutim': _(u'Services :: Lutim'),
                'frm_pix': _(u'Services :: Pix'),

                # autres labels gui
                'anonymous': _(u'Mode anonyme'),
                'optional_info': _(u'Informations facultatives'),
                'services': _(u'Services : '),
                
                # labels service jirafeau
                'jirafeau_one_time': _(u'Téléchargement unique'),
                'jirafeau_key': _(u'Clé : '),
                'jirafeau_time': _(u'Délai : '),
                # labels service lutim
                'lutim_crypt': _(u"Chiffrer l'image"),
                'lutim_first_view': _(u'Supprimer au premier accès'),
                'lutim_keep_exif': _(u'Garder les données EXIF'),
                'lutim_delete_days': _(u'Rétention : '),
                # labels service pix
                'pix_description': _(u"Description de l'image : "),
                'pix_password': _(u'Mot de passe : '),
                'pix_private': _(u'Image en mode privé'),
                'pix_pseudo': _(u'Pseudo : '),            
                'pix_tags': _(u'Tags (à séparer par une virgule) : '),
                # autres
                #'cat_img': _(u"Catégorie de l'image : "),
                #'resize_img': _(u'Redimensionnement : '), 

                # labels boutons
                'btn_cancel': _(u'Annuler'),
                'btn_close': _(u'Fermer'),
                'btn_ok': _(u'OK'),
                'btn_open': _(u'Ouvrir'),
                'btn_send': _(u'Envoyer'),
                'file_chooser_btn': _(u'_Parcourir'),

                'website': _(u'YUP.py Website'),
            },
            
            'none': _(u'Aucune'),
            
            # notebook
            'nb': {
                'conn': _(u'Connexion'),
                'serv': _(u"Services"),
            },

            'notify': {
                'cfg_created': _(u'Fichier de configuration créé !'),
                'cfg_saved': _(u'Fichier de config sauvegardé !'),
                'cfg_unsaved': _(u'Fichier de config non sauvegardé !'),
                'html': _(u'Ajout dans le fichier historique réussi !'),
                'img_name_not_match': 
                    _(u"Le nom d'image reçu semble ne pas correspondre")
                    + _(u' avec celui envoyé !'),
                'json_success_false':
                    _(u"JSON : Le service est en erreur : \n"),
                'json_unsuccess':
                    _(u"JSON : Le service est en échec \n")
                    + _(u"Il n'y a pas de retour d'informations !"),
                'no_img':
                    _(u"Vous n'avez pas choisi d'image. \n")
                    + _(u'Veuillez le faire !'),
                'not_int':
                    _(u"Le texte saisi n'est pas un chiffre entier. \n")
                    + _(u"Veuillez modifier votre saisie !"),
                'rep_req':
                    _(u'Réponse, de la requête au service,')
                    + _(u' enregistrée dans le fichier log !'),
            },

            'html': {
                'jirafeau': {
                    'infos': {
                    0: _(u"URL d'accès à l'image : "),
                    1: _(u"Afficher l'image : "),
                    2: _(u"Lien de téléchargement : "),
                    3: _(u"Lien de suppression : "),
                    4: _(u"Code pour insérer l'image dans le forum : "),
                    5: _(u"Code pour insérer l'image sur votre site web : "),
                    },
                    'param': {
                        1: {
                            0: _(u"Période d'enregistrement : "),
                            1: _(u'Effacement dès la première vue : '),
                            2: _(u'Clé : '),
                        },
                        2: {
                            0: _(u"Image gardée jusqu'au : "),
                        },
                    },
                },
                'lutim': {
                    'infos': {
                    0: _(u"URL d'accès à l'image : "),
                    1: _(u"Afficher l'image : "),
                    2: _(u"Code pour insérer la miniature dans le forum : "),
                    3: _(u"Lien de téléchargement : "),
                    4: _(u"Partage social : "),
                    5: _(u"Lien de suppression : "),
                    },
                    'param': {
                        1: {
                            0: _(u"Période d'enregistrement : "),
                            1: _(u'Effacement dès la première vue : '),
                            2: _(u'Garder les données EXIF : '),
                        },
                        2: {
                            0: _(u"Image gardée jusqu'au : "),
                        },
                    },
                },
                'pix': {
                    'infos': {
                    0: _(u"URL d'accès à l'image : "),
                    1: _(u"Afficher l'image : "),
                    2: _(u"Code pour insérer la miniature dans le forum : "),
                    3: _(u"Code pour insérer l'image dans le forum : "),
                    4: _(u"Code pour insérer la miniature ")
                        + _(u"sur votre site web : "),
                    5: _(u"Code pour insérer l'image sur votre site web : "),
                    },
                    'param': {
                        1: {
                            0: _(u'Informations facultatives : '),
                            1: _(u"Description de l'image : "),
                            2: _(u'Étiquettes : '),
                        },
                        2: {
                            0: _(u'Image en mode privé : '),
                        }
                    },
                },
                'h3': {
                    'infos': _(u'Informations retournées : '),
                    'param': _(u'Paramètres choisis : '),
                },
                'img_mini': _(u"Afficher cette image miniature : "),
                'url_historic': _(u'<em><a href="' + \
                    self.config['info']['Services']['logname'] + \
                    '">Voir l\'historique</a></em>'),
                'sended': _(u' envoyée sur '),
                'title': _(u'Image '),
                'to': _(u' à '),
            },

            'tooltip': {
                'can_send': _(u"Maintenant, vous pouvez envoyer votre image !"),
                'cant_send': _(u"Vous ne pouvez pas envoyer d'image")
                    + _(u" tant que vous n'en avez pas choisie !") ,
                'services': _(u"Choisissez un hébergeur d'images"),
            },
        }

        self.titles = {
            'html':
                _(NAME + u' :: ' + DESCR + u' :: ') +
                    self.texts['about']['historic'].title(),
            'main_window': _(NAME + u' :: ' + DESCR),
            'window_config': _(u'~ Configuration ~').title(),
            'window_edit': _(NAME + u' :: Fichier de configuration').title(),
            'window_help': _(NAME + u" :: Fichier d'aide").title(),
            'window_historic':
                _(NAME + u' :: Historique de téléversement').title(),
        }

        self.urls = {
            'git': 'https://framagit.org/hucste/YUP.py.git',
            'web': 'https://framagit.org/hucste/YUP.py/',
        }

        #self.process = None # to manage processus

        # Call logger
        self.log_me()
        self.log.info('[*** Launching Application YUP.py ***]')

        # Verify if personal config files, if not create...
        self.build_personals_files()

        # obtain infos into personal config files
        if os.path.isfile(self.files['config']):
            self.config['info'] = self.m_cfg.get_config2(self.files['config'])
            
        self.actions['default'] = self.config['info']['Services']['default']
        self.debug = self.config['info']['Main']['debug']
        
        self.log_level()
        
        self.rebuild_dict()

    def build_personals_files(self):
        '''Copy Config files into personal directory if not exists'''

        self.dict_for_cfg = dict()
        self.dict_for_cfg = {
            'config': self.config,
            #'debug': self.debug,
            'DESCR': DESCR,
            'files': self.files,
            'icon': self.files['images']['icon'],
            'log': self.log,
            'NAME': NAME,
            'text': self.texts,
            'title': self.titles,
            'url': self.urls,
        }
        self.m_cfg = Cfg(self.dict_for_cfg)
        self.m_cfg.build_personals_files()

    def log_level(self):
        '''Define Level Log'''

        if not self.debug:
            self.log.setLevel(logging.INFO)
        else:
            self.log.setLevel(logging.DEBUG)

    def log_me(self):
        ''' Logger '''

        dir_log = os.path.join(self.dir, 'log')
        if not os.path.exists(dir_log):
            os.makedirs(dir_log)
        del(dir_log)
        
        logging.config.fileConfig(self.files['log'])
        self.log = logging.getLogger('root')

    def rebuild_dict(self):
        ''' Rebuild dictionnary to remove index if active is false '''
        dict_for_tools = dict()    # dict for class Tools
        dict_for_tools = {
            'config': self.config,
            'debug': self.debug,
            'icon': self.files['images']['icon'],
            'log': self.log,
            'NAME': NAME,
            'title': self.titles,
        }
        self.tool = tool(dict_for_tools)
        self.actions = tool.rebuild_dict(self.actions)
        
        for key, val in enumerate(self.actions['index']):
            if 'days' in self.actions[val]:
                self.actions[val]['days'] = tool.rebuild_dict(
                    self.actions[val]['days'])
    
    def run(self):
        ''' Runner '''
        init = dict()
        init = {
            'actions': self.actions,
            'authors': self.authors,
            'cfg': self.config,
            'debug': self.debug,
            'DESCR': DESCR,
            'dir': self.dir,
            'files': self.files,
            'log': self.log,
            'm_cfg': self.m_cfg,    # pass module Config
            'NAME': NAME,
            'texts': self.texts,
            'titles': self.titles,
            'urls': self.urls,
            }
        
        gui = Gui(None, init)
        gui.mainloop()
    
if __name__ == "__main__":
    yup = App()
    yup.run()
