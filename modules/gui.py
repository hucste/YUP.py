#!/usr/bin/env python3

#
# # Authors informations
#
# @author: HUC Stéphane
# @email: <devs@stephane-huc.net>
# @url: http://stephane-huc.net
#
# @license : OpenBSD
#

'''Class Gui for YUP application'''

import os
import pprint

import tkinter as tk
import tkinter.filedialog as tkfd
import tkinter.ttk as ttk

import PIL.Image as pimg
import PIL.ImageTk as pimgtk


# Import Personals Modules
try:
    from modules.config import Config as Cfg
except ImportError:
    print(f'Cant import tool module onto Gui')
    sys.exit(1)
    
try:
    from modules.tooltip import CreateTooltip as tip
except ImportError:
    print(f'Cant import tooltip module onto Gui')
    sys.exit(1)

try:
    from modules.services import Services as Serv
except ImportError:
    print(f'Cant import services module onto Gui')
    sys.exit(1)

try:
    from modules.tools import Tools as tool
except ImportError:
    print(f'Cant import tools module onto Gui')
    sys.exit(1)

try:
    from modules.worker import Worker as work
except ImportError:
    print(f'Cant import Worker module onto Gui')
    sys.exit(1)
    

class Gui(tk.Tk):
    '''Building a Tkinter GUI'''

    def __init__(self, parent, init):
        tk.Tk.__init__(self, parent)
        self.parent = parent

        # building self
        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

        self.wrk_main = work(self)
        self.wrk_main.start()
        self.wrk_story = work(self)
        self.wrk_story.start()
        
        self.cursor = ''
        
        self.icon = self.files['images']['icon']
        self.logo = self.files['images']['logo']
        
        self.jrf_key = self.actions['jirafeau']['input_key']
        self.jrf_one_time_dl = self.actions['jirafeau']['one_time_dl']
        self.jrf_retention = self.actions['jirafeau']['days']['default']
        
        self.ltm_first_view = self.actions['lutim']['first_view']
        self.ltm_keep_exif = self.actions['lutim']['keep_exif']
        self.ltm_retention = self.actions['lutim']['days']['default']
        
        self.pix_descr = self.actions['toilelibre']['description']
        self.pix_private = self.actions['toilelibre']['private']
        self.pix_tags= self.actions['toilelibre']['tags']
        
        self.bits = 0
        self.size = 0

        dict_for_tools = dict()    # dict for class Tools
        dict_for_tools = {
            'config': self.cfg,
            'debug': self.debug,
            'icon': self.icon,
            'log': self.log,
            'NAME': self.NAME,
            'title': self.titles,
        }
        self.dict_for_tools = dict_for_tools
        self.tool = tool(dict_for_tools)

        self.ppp = pprint.PrettyPrinter(indent=4)
        self.styles()
        self.title(self.titles['main_window'])

        self.initialize()

    def about(self):
        '''Displaying a window about'''
        from modules.about import gui

        init = {
            'DESCR': self.DESCR, 
            'dir': self.dir,
            'icon': self.icon,
            'logo': self.logo,
            'NAME': self.NAME,
            'texts': self.texts,
            'urls': self.urls,
            }

        about = gui(None, init)
        about.title(self.titles['main_window'])
        about.mainloop()

    def byebye(self):
        '''Quit the app!'''
        self.quit()
        self.destroy()

    def display_icon(self):
        '''Manage icon'''
        img = pimg.open(self.logo)
        self.icon = pimgtk.PhotoImage(img)
        self.call('wm', 'iconphoto', self, self.icon)
    
    def initialize(self):
        '''initialize window'''
        self.grid()

        self.bind('<Control-q>', self._quit)

        self.display_icon()

        self.menu()
        self.gui()

        self.update()
        self.geometry(self.geometry())

    def fill_cbb(self, frame, service):
        '''Fill combobox for service lutim'''
        if service == 'framapic' or service == 'lutim':
            self.choice_lutim_days = tk.StringVar()
            
            days = self.tool.fill_tk_cbb(self.actions[service]['days'])
        
            self.cbb_lutim_days = ttk.Combobox(frame,
                                     textvariable=self.choice_lutim_days,
                                     values=days,
                                     state='readonly')
            self.cbb_lutim_days.grid(row=1, column=0)

            key = self.tool.array_search(self.actions[service]['days']['index'],
                                     self.actions[service]['days']['default'])
        
            self.choice_lutim_days.set(days[key])
            del(key, days)
            
            self.cbb_lutim_days.bind('<<ComboboxSelected>>',
                                self.on_cbb_days_clicked)
        
        elif service == 'jirafeau' or service == 'debianfacile':
            self.choice_jirafeau_days = tk.StringVar()
            
            days = self.tool.fill_tk_cbb(self.actions[service]['days'])
            
            self.cbb_jrf_days = ttk.Combobox(frame,
                                     textvariable=self.choice_jirafeau_days,
                                     values=days,
                                     state='readonly')
            self.cbb_jrf_days.grid(row=1, column=0)

            key = self.tool.array_search(self.actions[service]['days']['index'],
                                     self.actions[service]['days']['default'])
        
            self.choice_jirafeau_days.set(days[key])
            del(key, days)
            
            self.cbb_jrf_days.bind('<<ComboboxSelected>>',
                                                self.on_cbb_days_clicked)
        
    def gui(self):
        '''Displaying UI'''
        self.frm_main = tk.Frame(self, padx=20, pady=20)
        self.frm_main.grid(row=0, column=0)
        
        self.nb = ttk.Notebook(self.frm_main, name='nb')
        self.nb.enable_traversal()
        self.nb.grid(row=0, column=0)
        
        self.mng_frame_service()
        
        self.mng_frame_connection()
    
        ## Add an image logo
        self.mng_logo()

        ## Label for service        
        self.mng_gui_services()

        # Add a separator
        sep = ttk.Separator(self.frm_service)
        sep.grid(row=3, column=0, columnspan=3, sticky="ew", pady=10)

        ### Frame pix
        self.mng_frame_jirafeau()
        
        ### Frame lutim
        self.mng_frame_lutim()
        
        ### Frame pix
        self.mng_frame_pix()

        ### Frame Sendit
        self.mng_frame_send()

    def menu(self):
        '''Displaying menubar'''
        menubar = tk.Menu(self)

        menuFile = tk.Menu(menubar, tearoff=0)
        
        menuFile.add_command(label=self.texts['label']['menu_quit'],
                             underline=0, command=self.byebye,
                             accelerator="Ctrl+Q")

        menubar.add_cascade(label=self.texts['label']['menu_file'],
                            underline=0, menu=menuFile)

        menuHelp = tk.Menu(menubar, tearoff=0)
        menuHelp.add_command(label=self.texts['label']['menu_history'],
                             underline=0, command=self.open_historic)
        menuHelp.add_command(label=self.texts['label']['menu_about'],
                             underline=0, command=self.about)

        menubar.add_cascade(label=self.texts['label']['menu_help'],
                            underline=0, menu=menuHelp)

        self.config(menu=menubar)

    def mng_frame_connection(self):
        '''Displaying widgets into Frame Connexion'''
        # Frame for connexion informations
        self.frm_connexion = tk.Frame(self.nb, padx=20, pady=20)
        self.frm_connexion.grid(row=0, column=0)
        
        self.nb.add(self.frm_connexion, text=self.texts['label']['frm_conn'],
            underline=0, state="disabled")
        self.nb.hide(self.frm_connexion)
        
        ## Add Label Frame for connexions
        self.lbl_frm_conn = tk.LabelFrame(self.frm_connexion, padx=3, pady=3,
                                           text=self.texts['label']['frm_conn'])
        self.lbl_frm_conn.grid(row=5, column=0, columnspan=3)
       # self.lbl_frm_conn.configure(tk.DISABLED)

        self.bool_optional_conn = tk.IntVar()
        self.cbtn_optional_conn = tk.Checkbutton(self.lbl_frm_conn,
                                    command=self.on_cbtn_optional_conn_clicked,
                                    text=self.texts['label']['anonymous'],
                                    variable=self.bool_optional_conn)
        self.cbtn_optional_conn.select()
        self.cbtn_optional_conn.configure(state='disabled')
        self.cbtn_optional_conn.grid(row=0, column=0, columnspan=2, sticky=tk.W)

        # 
        self.lbl_pseudo = ttk.Label(self.lbl_frm_conn,
                                    text=self.texts['label']['pix_pseudo'])
        self.lbl_pseudo.grid(row=1, column=0, sticky=tk.W)
        self.lbl_pseudo.configure(state='disabled')

        self.txt_pseudo = ttk.Entry(self.lbl_frm_conn, width=10,
                                    textvariable=tk.StringVar())
        self.txt_pseudo.grid(row=1, column=1)
        self.txt_pseudo.configure(state='disabled')

        self.lbl_password = ttk.Label(self.lbl_frm_conn,
                                      text=self.texts['label']['pix_password'])
        self.lbl_password.grid(row=2, column=0, sticky=tk.W)
        self.lbl_password.configure(state='disabled')

        self.txt_password = ttk.Entry(self.lbl_frm_conn, width=10,
                                       textvariable=tk.StringVar())
        self.txt_password.grid(row=2, column=1)
        self.txt_password.configure(state='disabled')

    def mng_frame_jirafeau(self):
        '''Displaying widgets into Frame Jirafeau'''
        self.frm_jrf = tk.Frame(self.nb, padx=20, pady=20)
        self.frm_jrf.grid(row=0, column=0)
        
        self.nb.add(self.frm_jrf, text=self.texts['label']['frm_jrf'],
            underline=0, state="disabled")
        self.nb.hide(self.frm_jrf)
        
        self.lbl_jrf_days = ttk.Label(self.frm_jrf,
                                text=self.texts['label']['jirafeau_time'])
        self.lbl_jrf_days.grid(row=0, column=0, columnspan=2, sticky=tk.W)
        self.lbl_jrf_days.configure(state='disabled')

        self.fill_cbb(self.frm_jrf, 'jirafeau')
        
        self.cbb_jrf_days.configure(state='disabled')
        
        self.bool_jrf_one_time = tk.IntVar()
        self.cbtn_jrf_one_time = tk.Checkbutton(self.frm_jrf,
                                command=self.on_cbtn_jrf_one_time_clicked,
                                text=self.texts['label']['jirafeau_one_time'],
                                variable=self.bool_jrf_one_time)
                                
        self.cbtn_jrf_one_time.grid(row=2, column=0, columnspan=2, sticky=tk.W)
        self.cbtn_jrf_one_time.configure(state='disabled')
        
        self.lbl_jrf_key = ttk.Label(self.frm_jrf,
                                    text=self.texts['label']['jirafeau_key'])
        self.lbl_jrf_key.grid(row=3, column=0, sticky=tk.W)
        self.lbl_jrf_key.configure(state='disabled')

        self.txt_jrf_key = ttk.Entry(self.frm_jrf, width=10,
                                       textvariable=tk.StringVar())
        self.txt_jrf_key.grid(row=4, column=0, columnspan=2)
        self.txt_jrf_key.configure(state='disabled')

    def mng_frame_lutim(self):
        '''Displaying widgets into Frame Lutim'''
        #self.frm_lutim = ttk.Frame(self.frm_service)
        #self.frm_lutim.grid(row=4, column=1)
        self.frm_lutim = tk.Frame(self.nb, padx=20, pady=20)
        self.frm_lutim.grid(row=0, column=0)
        
        self.nb.add(self.frm_lutim, text=self.texts['label']['frm_lutim'],
            underline=0, state="disabled")
        self.nb.hide(self.frm_lutim)

        self.lbl_lutim_days = ttk.Label(self.frm_lutim,
                                text=self.texts['label']['lutim_delete_days'])
        self.lbl_lutim_days.grid(row=0, column=0, sticky=tk.W)
        self.lbl_lutim_days.configure(state='disabled')

        self.fill_cbb(self.frm_lutim, 'lutim')
        
        self.cbb_lutim_days.configure(state='disabled')
        
        self.bool_ltm_first_view = tk.IntVar()
        self.cbtn_ltm_first_view = tk.Checkbutton(self.frm_lutim,
                                command=self.on_cbtn_ltm_first_view_clicked,
                                text=self.texts['label']['lutim_first_view'],
                                variable=self.bool_ltm_first_view)
        #self.cbtn_ltm_first_view.select()
        self.cbtn_ltm_first_view.grid(row=2, column=0, sticky=tk.W)
        self.cbtn_ltm_first_view.configure(state='disabled')

        self.bool_ltm_keep_exif = tk.IntVar()
        self.cbtn_ltm_keep_exif = tk.Checkbutton(self.frm_lutim,
                                command=self.on_cbtn_ltm_keep_exif_clicked,
                                text=self.texts['label']['lutim_keep_exif'],
                                variable=self.bool_ltm_keep_exif)
        #self.cbtn_ltm_keep_exif.select()
        self.cbtn_ltm_keep_exif.grid(row=3, column=0, sticky=tk.W)
        self.cbtn_ltm_keep_exif.configure(state='disabled')
        
    def mng_frame_pix(self):
        '''Displaying widgets into Frame Pix'''
        #self.frm_pix = tk.Frame(self.frm_service, padx=3, pady=3)
        #self.frm_pix.grid(row=4, column=0)
        self.frm_pix = tk.Frame(self.nb, padx=20, pady=20)
        self.frm_pix.grid(row=0, column=0)
        
        self.nb.add(self.frm_pix, text=self.texts['label']['frm_pix'],
            underline=0, state="disabled")
        self.nb.hide(self.frm_pix)

        ## Add Label frame for optionals informations
        self.lbl_frm_info = tk.LabelFrame(self.frm_pix, padx=3, pady=3,
                                           text=self.texts['label']['frm_info'])
        self.lbl_frm_info.grid(row=4, column=0, columnspan=2)

        ### Check button for optional information
        self.bool_optional_info = tk.IntVar()
        self.cbtn_optional_info = tk.Checkbutton(self.lbl_frm_info,
                                    command=self.on_cbtn_optional_info_clicked,
                                    text=self.texts['label']['optional_info'],
                                    variable=self.bool_optional_info)
        self.cbtn_optional_info.select()
        self.cbtn_optional_info.grid(row=0, column=0, columnspan=2, sticky=tk.W)
        self.cbtn_optional_info.configure(state='disabled')

        # labels for pix informations
        self.lbl_img_descr = ttk.Label(self.lbl_frm_info,
                                    text=self.texts['label']['pix_description'])
        self.lbl_img_descr.grid(row=1, column=0, sticky=tk.W)
        self.lbl_img_descr.configure(state='disabled')

        self.txt_img_descr = ttk.Entry(self.lbl_frm_info, width=10,
                                       textvariable=tk.StringVar())
        self.txt_img_descr.grid(row=1, column=1)
        self.txt_img_descr.configure(state='disabled')

        self.lbl_img_tags = ttk.Label(self.lbl_frm_info,
                                      text=self.texts['label']['pix_tags'])
        self.lbl_img_tags.grid(row=2, column=0, sticky=tk.W)
        self.lbl_img_tags.configure(state='disabled')

        self.txt_img_tags = ttk.Entry(self.lbl_frm_info, width=10,
                                      textvariable=tk.StringVar())
        self.txt_img_tags.grid(row=2, column=1)
        self.txt_img_tags.configure(state='disabled')

        self.bool_img_private = tk.IntVar()
        self.cbtn_img_private = tk.Checkbutton(self.lbl_frm_info,
                                    command=self.on_cbtn_img_private_clicked,
                                    text=self.texts['label']['pix_private'],
                                    variable=self.bool_img_private)
        self.cbtn_img_private.grid(row=3, column=0, columnspan=2, sticky=tk.W)
        self.cbtn_img_private.configure(state='disabled')

    def mng_frame_send(self):
        '''Displaying widgets to Frame Send'''
        self.width = self.frm_service.winfo_width()
        self.frm_send = tk.Frame(self.frm_service, padx=3, pady=10)
        #                         width=self.width)
        self.frm_send.grid(row=6, column=0, columnspan=2)

        ## Button to send datas
        self.btn_send = ttk.Button(self.frm_send,
                                   text=self.texts['label']['btn_send'],
                                   command=self.on_btn_send_clicked)
        self.btn_send.grid(row=2, column=0, columnspan=2)
        self.btn_send.configure(state='disabled')
        
        #tip(self.btn_send, self.texts['tooltip']['cant_send'])

    def mng_frame_service(self):
        '''Displaying widgets to Frame Service'''
        # Frame for service informations
        self.frm_service = tk.Frame(self.nb, padx=20, pady=20)
        self.frm_service.grid(row=0, column=0)

        self.nb.add(self.frm_service, text=self.texts['nb']['serv'],
            underline=0)

    def mng_gui_services(self):
        '''Displaying widgets to select services'''
        lbl_service = tk.Label(self.frm_service, 
                                text=self.texts['label']['services'])
        lbl_service.config(padx=7)
        lbl_service.grid(row=0, column=1, sticky=tk.W)

        ## Combobox for service
        self.choice_service = tk.StringVar()
        services = self.tool.fill_tk_cbb(self.actions)
        self.cbb_service = ttk.Combobox(self.frm_service,
                                        textvariable=self.choice_service,
                                        values=services, 
                                        state='readonly')
        self.cbb_service.grid(row=1, column=1)
        
        self.cbb_service.bind('<<ComboboxSelected>>',
                              self.on_cbb_service_clicked)

        ## Botton to choice needed image
        self.btn_browse = ttk.Button(self.frm_service,
                                     text=self.texts['label']['file_chooser'],
                                     command=self.on_btn_browse_clicked)
        self.btn_browse.grid(row=2, column=1)
        self.btn_browse.configure(state='disabled')
        
        #tip(self.cbb_service, self.texts['tooltip']['services'])

    def mng_logo(self):
        '''Displaying logo'''
        logo = pimg.open(self.logo)
        self.logo = pimgtk.PhotoImage(logo)
        img_service = tk.Label(self.frm_service, image=self.logo, height=30)
        #img_service.config(padx=20, pady=20, relief=tk.SUNKEN)
        img_service.grid(row=0, column=0, rowspan=3)

    def on_btn_browse_clicked(self):
        ## Add a filedialog
        img_exts = ('.gif', '.jpg', '.png')
        self.pixxie = tkfd.askopenfilename(initialdir=(os.path.expanduser('~')),
                                    filetypes=[('Images', img_exts)],
                                    title=self.texts['label']['file_chooser'],
                                    parent=self)
        
        if(self.pixxie):
            if 'max_file_size' in self.actions[self.service]:
                if tool.check_size(self.pixxie, 
                    self.actions[self.service]['max_file_size']):
                        self.btn_send.configure(state='enabled')
                        #(self.btn_send, self.texts['tooltip']['can_send'])
            
            else:
                self.btn_send.configure(state='enabled')
                #tip(self.btn_send, self.texts['tooltip']['can_send'])

    def on_btn_send_clicked(self):
        ''' Build informations for datas'''
        #self.wrk_main.start()
        self.wrk_main.mng_cursor()

        if self.service == 'debianfr' or self.service == 'toilelibre':
            self.pix_descr = self.txt_img_descr.get()
            self.pix_private = self.bool_img_private.get()
            self.pix_tags = self.txt_img_tags.get()
        
        elif self.service == 'jirafeau':
            self.jrf_key = self.txt_jrf_key.get()

        #elif self.service == 'framapic' or self.service == 'lutim':
            #self.ltm_first_view = self.cbtn_ltm_first_view.get()
            #self.ltm_keep_exif = self.cbtn_ltm_keep_exif.get()
        
        self.btn_send.configure(state='disabled')
        self.bool_optional_info.set(1)
        self.lbl_img_descr.configure(state='disabled')
        self.txt_img_descr.configure(state='disabled')
        self.txt_img_descr.delete(0,'end')
        self.lbl_img_tags.configure(state='disabled')
        self.txt_img_tags.configure(state='disabled')
        self.txt_img_tags.delete(0, 'end')

        self.send_datas()

    def on_cbb_days_clicked(self, event):
        '''Get value day'''
        if self.service == "framapic" or self.service == "lutim":
            self.ltm_retention = self.tool.array_search_by_label2(
                self.actions[self.service]['days'], self.cbb_lutim_days.get())
                
        elif self.service == "jirafeau" or self.service == 'debianfacile':
            self.jrf_retention = self.tool.array_search_by_label2(
                self.actions[self.service]['days'], self.cbb_jrf_days.get())
        
    def on_cbb_service_clicked(self, event):
        '''Get choosed value service'''
        #self.service = self.cbb_service.get()
        self.service = self.tool.array_search_by_label2(self.actions,
            self.cbb_service.get())
        self.btn_browse.configure(state='enabled')

        if self.service == 'debianfr' or self.service == 'toilelibre':
            self.cbb_jrf_days.configure(state='disabled')
            self.cbb_lutim_days.configure(state='disabled')

            self.cbtn_ltm_first_view.configure(state='disabled')
            self.cbtn_img_private.configure(state='disabled')
            self.cbtn_ltm_keep_exif.configure(state='disabled')
            self.cbtn_jrf_one_time.configure(state='disabled')
            self.cbtn_optional_conn.configure(state='normal')
            self.cbtn_optional_info.configure(state='normal')

            self.lbl_jrf_days.configure(state='disabled')
            self.lbl_lutim_days.configure(state='disabled')

            self.on_cbtn_optional_info_clicked()
            self.on_cbtn_optional_conn_clicked()
            
            self.txt_jrf_key.configure(state='disabled')
            
            self.nb.hide(self.frm_jrf)
            self.nb.hide(self.frm_lutim)
            #self.nb.add(self.frm_lutim, state="disabled")
            self.nb.add(self.frm_pix, state="normal",
                text=self.actions[self.service]['label'] + ' :: ' \
                + self.texts['nb']['serv'])

        elif self.service == 'framapic' or self.service == 'lutim':
            self.fill_cbb(self.frm_lutim, self.service)
            
            self.cbb_jrf_days.configure(state='disabled')
            self.cbb_lutim_days.configure(state='readonly')
            
            self.cbtn_ltm_first_view.configure(state='normal')
            self.cbtn_img_private.configure(state='disabled')
            self.cbtn_ltm_keep_exif.configure(state='normal')
            self.cbtn_jrf_one_time.configure(state='disabled')
            self.cbtn_optional_conn.configure(state='disabled')
            self.cbtn_optional_info.configure(state='disabled')

            self.lbl_img_descr.configure(state='disabled')
            self.lbl_img_tags.configure(state='disabled')
            self.lbl_jrf_days.configure(state='disabled')
            self.lbl_lutim_days.configure(state='normal')
            self.lbl_password.configure(state='disabled')
            self.lbl_pseudo.configure(state='disabled')

            self.txt_img_descr.configure(state='disabled')
            self.txt_img_tags.configure(state='disabled')
            self.txt_jrf_key.configure(state='disabled')
            self.txt_password.configure(state='disabled')
            self.txt_pseudo.configure(state='disabled')
            
            self.nb.hide(self.frm_jrf)
            self.nb.add(self.frm_lutim, state="normal",
                text=self.actions[self.service]['label'] + ' :: ' \
                + self.texts['nb']['serv'])
            self.nb.hide(self.frm_pix)
            #self.nb.add(self.frm_pix, state="disabled")
        
        elif self.service == 'jirafeau' or self.service == 'debianfacile':
            self.fill_cbb(self.frm_jrf, self.service)
            
            self.cbb_jrf_days.configure(state='readonly')
            self.cbb_lutim_days.configure(state='disabled')

            self.cbtn_ltm_first_view.configure(state='disabled')
            self.cbtn_img_private.configure(state='disabled')
            self.cbtn_ltm_keep_exif.configure(state='disabled')
            self.cbtn_jrf_one_time.configure(state='normal')
            self.cbtn_optional_conn.configure(state='disabled')
            self.cbtn_optional_info.configure(state='disabled')
            
            self.lbl_img_descr.configure(state='disabled')
            self.lbl_img_tags.configure(state='disabled')
            self.lbl_jrf_days.configure(state='normal')
            self.lbl_jrf_key.configure(state='normal')
            self.lbl_lutim_days.configure(state='disabled')
            self.lbl_password.configure(state='disabled')
            self.lbl_pseudo.configure(state='disabled')

            self.txt_img_descr.configure(state='disabled')
            self.txt_img_tags.configure(state='disabled')
            self.txt_jrf_key.configure(state='normal')
            self.txt_password.configure(state='disabled')
            self.txt_pseudo.configure(state='disabled')
            
            self.nb.add(self.frm_jrf, state="normal",
                text=self.actions[self.service]['label'] + ' :: ' \
                + self.texts['nb']['serv'])
            self.nb.hide(self.frm_lutim)
            self.nb.hide(self.frm_pix)
            #self.nb.add(self.frm_pix, state="disabled")

    def on_cbtn_ltm_first_view_clicked(self):
        '''Get value checkbutton first-view'''
        self.ltm_first_view = self.bool_ltm_first_view.get()
        
    def on_cbtn_img_private_clicked(self):
        '''Get value checkbutton private'''
        self.pix_private = self.bool_img_private.get()
        
    def on_cbtn_ltm_keep_exif_clicked(self):
        '''Get value checkbutton keep-exif'''
        self.ltm_keep_exif = self.bool_ltm_keep_exif.get()
        
    def on_cbtn_jrf_one_time_clicked(self):
        '''Get value checkbutton one_time'''
        self.jrf_one_time_dl = self.bool_jrf_one_time.get()

    def on_cbtn_optional_conn_clicked(self):
        '''Get value checkbutton optional_conn'''
        self.optional_conn = self.bool_optional_conn.get()
        
        if self.optional_conn == 0:
            self.lbl_password.configure(state='normal')
            self.lbl_pseudo.configure(state='normal')

            self.txt_password.configure(state='normal')
            self.txt_pseudo.configure(state='normal')
            
        else:
            self.lbl_password.configure(state='disabled')
            self.lbl_pseudo.configure(state='disabled')

            self.txt_password.configure(state='disabled')
            self.txt_pseudo.configure(state='disabled')
            
    def on_cbtn_optional_info_clicked(self):
        '''Get value checkbutton optional_info'''
        self.pix_optional_info = self.bool_optional_info.get()
        
        if self.pix_optional_info == 0:
            self.lbl_img_descr.configure(state='normal')
            self.lbl_img_tags.configure(state='normal')
            
            self.txt_img_descr.configure(state='normal')
            self.txt_img_tags.configure(state='normal')
            
            self.cbtn_img_private.configure(state='normal')
            
        else:
            self.lbl_img_descr.configure(state='disabled')
            self.lbl_img_tags.configure(state='disabled')
            
            self.txt_img_descr.configure(state='disabled')
            self.txt_img_tags.configure(state='disabled')
            
            self.cbtn_img_private.configure(state='disabled')

    def open_historic(self):
        '''Display historic file html'''
        #self.wrk_story.start()
        self.wrk_story.set_init(self.dict_for_tools)
        self.wrk_story.display_historic(self.cfg['info']['Services']['logname'])

    def send_datas(self):
        '''Push all datas to hosting service'''
        self.log.info('=> Push Image to service: %s' % self.service)

        self.cfg['info'] = self.m_cfg.get_config2(self.files['config'])

        init = {
            'actions': self.actions,
            'config': self.cfg,
            'debug': self.debug,
            'DESCR': self.DESCR,
            'dir': self.dir,
            'files': self.files,
            'icon': self.files['images']['icon'],
            'log': self.log,
            'NAME': self.NAME,
            'service': self.service,
            'text': self.texts,
            'title': self.titles,
            'url': self.urls,
        }

        datas = {
            'image': self.pixxie,
        }

        if self.service == "framapic" or self.service == "lutim":

            # data crypt: always true
            datas['crypt'] = self.actions['lutim']['crypt']

            # data delete-day
            if self.ltm_retention is None or self.ltm_retention == '':
                datas['delete-day'] = self.actions['lutim']['retention']
            else:
                datas['delete-day'] = self.ltm_retention

            # data first-view
            if self.ltm_first_view is None or self.ltm_first_view == '':
                datas['first-view'] = self.actions['lutim']['first_view']
            else:
                datas['first-view'] = self.ltm_first_view

            # data keep-exit
            if self.ltm_keep_exif is None or self.ltm_keep_exif == '':
                datas['keep-exif'] = self.actions['lutim']['keep_exif']
            else:
                datas['keep-exif'] = self.ltm_keep_exif

            # data format
            datas['format'] = self.cfg['info']['lutim']['format_output']

        elif self.service == 'debianfr' or self.service == 'toilelibre':

            # data description
            if self.pix_descr is None or self.pix_descr == '':
                datas['description'] = self.actions['toilelibre']['description']
            else:
                datas['description'] = self.pix_descr

            # data max file size
            datas['MAX_FILE_SIZE'] = self.actions[self.service]['max_file_size']

            # data private
            if self.pix_private is None or self.pix_private == '':
                datas['private'] = self.actions['toilelibre']['private']
            else:
                datas['private'] = self.pix_private

            # data tags
            if self.pix_tags is None or self.pix_tags == '':
                datas['tags'] = self.actions['toilelibre']['tags']
            else:
                datas['tags'] = self.pix_tags
            
            datas['optional_info'] = self.pix_optional_info
            
        elif self.service == 'jirafeau' or self.service == 'debianfacile':
            
            # data max file size
            datas['max_file_size'] = self.actions[self.service]['max_file_size']
            
            if self.jrf_one_time_dl is None or self.jrf_one_time_dl == '':
                datas['one_time_download'] = self.actions['jirafeau']['one_time_dl']
            else:
                datas['one_time_download'] = self.jrf_one_time_dl
            
            if self.jrf_key is None or self.jrf_key == '':
                datas['key'] = self.actions['jirafeau']['input_key']
            else:
                datas['key'] = self.jrf_keys
            
            if self.jrf_retention is None or self.jrf_retention == '':
                datas['time'] = self.actions[self.service]['day']['default']
            else:
                datas['time'] = self.jrf_retention

        #self.ppp.pprint(datas)
        
        if self.debug:
            self.log.debug('Datas sending to the service:')
            self.log.debug(datas)
        
        self.wrk_main.set_init(init)
        self.wrk_main.set_datas(datas)
        
        # initialize sending hosting service
        if self.wrk_main.send_hosting():
            self.wrk_main.HTMLWorker()
            self.wrk_main.display_historic(self.files['html'])
    
        self.wrk_main.restore_cursor()

        del(datas)

    def styles(self):
        '''Define new styles'''
        # Themes, Styles
        self.style = ttk.Style()
        try:
            self.style.theme_use(self.cfg['info']['Main']['theme'])
        except tk.TclError:
            self.style.theme_use('default')

        #self.tool.getAttributes()
        #print(f'style: {el}')
        #self.style.configure('TFrame', background="yellow")
        self.style.configure('TLabelFrame', borderwidth=0, background='green',
                        padding=10, relief='flat')
        self.style.configure('Red.TButton', background="red")
        self.style.configure('Green.TButton', background="green")
    
    def _quit(self, event):
        self.byebye()
