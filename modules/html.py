#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

#
# # Authors informations
#
# @author: HUC Stéphane
# @email: <devs@stephane-huc.net>
# @url: http://stephane-huc.net
#
# @license : OpenBSD
#

''' Manage HTML '''

from bs4 import BeautifulSoup
import html
import shutil

# Import Personals Modules
try:
    from modules.tools import Tools as tool
except ImportError:
    print(f'Cant import tools module onto Gui')
    sys.exit(1)


class ToolsHTML(object):
    ''' Tools to manage HTML '''
    
    def __init__(self, init):

        # building self
        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])
                
        self.urls = list()
    
    def build_html(self, filename):
        '''Build Code HTML to display into Historic'''

        self.log.info('*** Services :: Build Code HTML :')
        
        article_id = self.image['basename'].lower().replace(' ', '_') + \
            '_' + str(self.image['timestamp'])
        
        # manage link of article
        if filename == self.config['info']['Services']['logname']:
            self.link = self.soup.new_tag('li')
            a = self.soup.new_tag('a')
            a['href'] = '#' + article_id
            a.string = self.image['basename']
            self.link.append(a)
        
        self.article = self.soup.new_tag('article')
        self.article['id'] = article_id
        
        del(article_id)
        
        header = self.soup.new_tag('header')
        h2 = self.soup.new_tag('h2')
        strong = self.soup.new_tag('strong')
        a = self.soup.new_tag('a')
        a['href'] = self.image['file']
        a.string = self.image['basename'] 
        strong.append(a)
        em = self.soup.new_tag('em')
        em.string = self.actions[self.service]['label']
        
        h2.append(self.text['html']['title'])
        h2.append(strong)
        h2.append(self.text['html']['sended'])
        h2.append(em)
        header.append(h2)
        
        time = self.soup.new_tag('time')
        time['datetime'] = self.image['time_info']
        time.string = self.image['time_human'] 
        header.append(time)
        
        self.article.append(header)
        
        div_container = self.soup.new_tag('div')
        div_container['class'] = 'container-fluid'
        div_row = self.soup.new_tag('div')
        div_row['class'] = 'row'
        div_col3 = self.soup.new_tag('div')
        div_col3['class'] = 'col-sm-3'
        
        h3 = self.soup.new_tag('h3')
        h3.string = self.text['html']['h3']['param']
        div_col3.append(h3)
        
        self.ul = self.soup.new_tag('ul')
        
        if self.service == 'framapic' or self.service == 'lutim':
            
            self.build_html_params_article_ltm()
            
        elif self.service == 'debianfr' or self.service == 'toilelibre':
            
            self.build_html_params_article_pix()
        
        elif self.service == 'jirafeau' or self.service == "debianfacile":
            
            self.build_html_params_article_jrf()
        
        div_col3.append(self.ul)
        
        hr = self.soup.new_tag('hr')
        div_col3.append(hr)

        try:
            div_col3.append(self.ul2)
            div_col3.append(self.hr2)
        except AttributeError:
            pass
        
        div_row.append(div_col3)
        ### end bloc param
        
        # manage bloc to display informations about image      
        div_col9 = self.soup.new_tag('div')
        div_col9['class'] = 'col-sm-9 brd-l'
        
        h3 = self.soup.new_tag('h3')
        h3.string = self.text['html']['h3']['infos']
        
        div_col9.append(h3)
        
        self.dl = self.soup.new_tag('dl')
        
        if self.service == 'framapic' or self.service == 'lutim':

            self.build_html_image_ltm()

        elif self.service == 'debianfr' or self.service == 'toilelibre':

            self.build_html_image_pix()
            
        elif self.service == 'jirafeau' or self.service == "debianfacile":
            
            self.build_html_image_jrf()
        
        ## end bloc info
        div_col9.append(self.dl)

        # manage end of article
        div_row.append(div_col9)
        div_container.append(div_row)
        self.article.append(div_container)

    def build_html_image_jrf(self):
        '''Jirafeau: Build Html to display image's informations into article'''
        for key, val in enumerate(self.urls):
                
                self.log.debug('Key of self.urls: %s' % key)
                self.log.debug('Value of self.urls: %s' % val)
                
                if key == 0:
                    dt = self.soup.new_tag('dt')
                    a = self.soup.new_tag('a')
                    a['href'] = val
                    a['title'] = self.image['basename']
                    
                    img = self.soup.new_tag('img')
                    img['alt'] = self.image['basename']
                    img['src'] = self.image['file']
                    img['width'] = str(
                        self.config['info']['Services']['mini_width'])
                    
                    a.append(img)
                    dt.append(a)
                    self.dl.append(dt)
                
                elif key == 4 or key == 5:
                    # encode htmlentities
                    encoded = html.escape(val, quote=True)
                    self.log.debug('Value encoded: %s' % encoded)
                    # get size
                    size = str(len(val))
                    self.log.debug('Size: %s' % size)

                    dd = self.soup.new_tag('dd')
                    if key % 2:
                        dd['class'] = 'layout'
                    
                    dd.string = self.text['html']['jirafeau']['infos'][key]
                    
                    br = self.soup.new_tag('br')
                    h_input = self.soup.new_tag('input')
                    h_input['readonly'] = 'readonly'
                    h_input['size'] = size
                    h_input['type'] = 'text'
                    h_input['value'] = encoded
                    
                    dd.append(br)
                    dd.append(h_input)
                    self.dl.append(dd)
                    
                    del(encoded, size)
                
                else:
                    dd = self.soup.new_tag('dd')
                    if key % 2:
                        dd['class'] = 'layout'
                    
                    dd.string = self.text['html']['jirafeau']['infos'][key]
                    
                    strong = self.soup.new_tag('strong')
                    a = self.soup.new_tag('a')
                    a['href'] = val
                    a['title'] = self.image['basename']
                    a.string = val
                    strong.append(a)
                    
                    dd.append(strong)
                    self.dl.append(dd)

    def build_html_image_ltm(self):
        '''Lutim: Build html to display image's informations into article'''
        for key, val in enumerate(self.urls):
                
                self.log.debug('Key of self.urls: %s' % key)
                self.log.debug('Value of self.urls: %s' % val)

                if key == 0:
                    dt = self.soup.new_tag('dt')
                    a = self.soup.new_tag('a')
                    a['href'] = val
                    a['title'] = self.image['basename']
                    img = self.soup.new_tag('img')
                    img['alt'] = self.image['basename']
                    img['src'] = self.image['file']
                    img['width'] = str(
                        self.config['info']['Services']['mini_width'])
                    
                    a.append(img)
                    dt.append(a)
                    self.dl.append(dt)
                        
                elif key == 2:
                    # encode htmlentities
                    encoded = html.escape(val, quote=True)
                    self.log.debug('Value encoded: %s' % encoded)
                    # get size
                    size = str(len(val))
                    self.log.debug('Size: %s' % size)

                    dd = self.soup.new_tag('dd')
                    if key % 2:
                        dd['class'] = 'layout'
                    
                    br = self.soup.new_tag('br')
                    h_input = self.soup.new_tag('input')
                    h_input['readonly'] = 'readonly'
                    h_input['size'] = size
                    h_input['type'] = 'text'
                    h_input['value'] = encoded
                    
                    dd.append(self.text['html']['lutim']['infos'][key])
                    dd.append(br)
                    dd.append(h_input)
                    self.dl.append(dd)
                    
                    del(encoded, size)
                        
                else: 
                    dd = self.soup.new_tag('dd')
                    if key % 2:
                        dd['class'] = 'layout'
                    
                    strong = self.soup.new_tag('strong')
                    a = self.soup.new_tag('a')
                    a['href'] = val
                    a['title'] = self.image['basename']
                    a.string = val
                    
                    strong.append(a)
                    dd.append(self.text['html']['lutim']['infos'][key])
                    dd.append(strong)
                    self.dl.append(dd)

    def build_html_image_pix(self):
        '''Pix: Build Html to display image's informations into article'''
        for key, val in enumerate(self.urls):
                
                self.log.debug('Key of self.urls: %s' % key)
                self.log.debug('Value of self.urls: %s' % val)

                if key == 0:
                    tag = 'dt'
                else:
                    tag = 'dd'

                if key == 0 or key == 1:
                    tag = self.soup.new_tag(tag)
                    if key % 2:
                        tag['class'] = 'layout'
                    
                    tag.string = self.text['html']['pix']['infos'][key]
                    
                    strong = self.soup.new_tag('strong')
                    a = self.soup.new_tag('a')
                    a['href'] = val
                    a['title'] = self.text['html']['pix']['infos'][key] \
                        + self.image['basename']
                    a.string = val
                    strong.append(a)
                    
                    tag.append(strong)
                    
                    self.dl.append(tag)

                elif key == 2 or key == 3:
                    # encode htmlentities
                    encoded = html.escape(val, quote=True)
                    self.log.debug('Value encoded: %s' % encoded)
                    # get size
                    size = str(len(val))
                    self.log.debug('Size: %s' % size)
                    
                    dd = self.soup.new_tag('dd')
                    if key % 2:
                        dd['class'] = 'layout'
                    
                    dd.string = self.text['html']['pix']['infos'][key]
                    
                    br = self.soup.new_tag('br')
                    h_input = self.soup.new_tag('input')
                    h_input['readonly'] = 'readonly'
                    h_input['size'] = size
                    h_input['type'] = 'text'
                    h_input['value'] = encoded
                    
                    dd.append(br)
                    dd.append(h_input)

                    self.dl.append(dd)
                    
                    self.log.debug('HTML: %s' % self.article)
                    del(encoded, size)

                elif key == 4 or key == 5:
                    # encode htmlentities
                    encoded = html.escape(val, quote=True)
                    self.log.debug('Value encoded: %s' % encoded)
                    # get size
                    size = str(len(val))
                    self.log.debug('Size: %s' % size)
                    
                    dd = self.soup.new_tag('dd')
                    if key % 2:
                        dd['class'] = 'layout'
                    
                    dd.string = self.text['html']['pix']['infos'][key]
                    
                    br = self.soup.new_tag('br')
                    h_input = self.soup.new_tag('input')
                    h_input['readonly'] = 'readonly'
                    h_input['size'] = size
                    h_input['type'] = 'text'
                    h_input['value'] = encoded
                    
                    dd.append(br)
                    dd.append(h_input)
                    dd.append(br)
                    dd.append(val)
                    
                    self.dl.append(dd)
                    
                    self.log.debug('HTML: %s' % self.article)
                    del(encoded, size)

                else:
                    dd = self.soup.new_tag('dd')
                    if key % 2:
                        dd['class'] = 'layout'
                    
                    dd.string = self.text['html']['pix']['infos'][key]
                    
                    br = self.soup.new_tag('br')
                    strong = self.soup.new_tag('strong')
                    strong.string = val
                    
                    dd.append(br)
                    dd.append(strong)
                    
                    self.dl.append(dd)

    def build_html_params_article_jrf(self):
        '''Jirafeau: Build Html to display paramaters into article'''
        for key, val in self.text['html']['jirafeau']['param'][1].items():
            
                li = self.soup.new_tag('li')
                li.string = val
                
                strong = self.soup.new_tag('strong')
                
                if key == 0: 
                    nb = self.datas['time']
                    serv = self.service
                    strong.string = self.actions[serv]['days'][nb]['label']
                    del(nb, serv)
                
                elif key == 1:
                    boolean = self.datas['one_time_download']
                    strong.string = self.text['bool'][boolean]['label']
                    del(boolean)
                
                elif key == 2:
                    if self.datas['key'] is None \
                        or self.datas['key'] == '':
                        strong.string = self.text['none']
                    
                    else:
                        strong.string = self.datas['key']
                
                li.append(strong)
                self.ul.append(li)

    def build_html_params_article_ltm(self):
        '''Lutim: Build Html to display parameters into article'''
        for key, val in self.text['html']['lutim']['param'][1].items():

                li = self.soup.new_tag('li')
                li.append(val)
                strong = self.soup.new_tag('strong')
            
                if key == 0: 
                    nb = self.datas['delete-day']
                    serv = self.service
                    strong.string = self.actions[serv]['days'][nb]['label']
                    del(nb, serv)
                
                else:
                    if key == 1: 
                        boolean = self.datas['first-view']
                    elif key == 2:
                        boolean = self.datas['keep-exif']
                        
                    strong.string = self.text['bool'][boolean]['label']
                    del(boolean)
                
                li.append(strong)
                self.ul.append(li)

    def build_html_params_article_pix(self):
        '''Pix: Build Html to display parameters into article'''
        for key, val in self.text['html']['pix']['param'][1].items():

                li = self.soup.new_tag('li')
                li.append(val)
                strong = self.soup.new_tag('strong')
            
                if key == 0: 
                    boolean = self.datas['optional_info']
                    
                    if boolean == 0:
                        boolean = 1
                    else:
                        boolean = 0
                    
                    strong.string = self.text['bool'][boolean]['label']
                    del(boolean)
                
                elif key == 1:
                    if self.datas['description'] is None \
                        or self.datas['description'] == '':
                        text = self.text['none']
                    
                    else:
                        text = self.datas['description']

                    strong.string = text
                    del(text)
                
                elif key == 2:
                    if self.datas['tags'] is None \
                        or self.datas['tags'] == '':
                        text = self.text['none']
                    
                    else:
                        text = self.datas['tags']

                    strong.string = text
                    del(text)

                li.append(strong)
                self.ul.append(li)
            
        if(self.text['html']['pix']['param'][2]):

            self.hr2 = self.soup.new_tag('hr')
            self.ul2 = self.soup.new_tag('ul')
                
            for key, val in self.text['html']['pix']['param'][2].items():
                
                li = self.soup.new_tag('li')
                
                strong = self.soup.new_tag('strong')
                strong.string = self.text['bool'][self.datas['private']]['label']
                
                li.append(val)
                li.append(strong)
                self.ul2.append(li)
    
    def modify(self, filename):
        '''  Modify code HML '''
        
        if filename == self.files['html']:
            for article in self.soup.findAll("article"):
                article.extract()
        
        self.soup.html['lang'] = self.config['lang']
        self.soup.head.title.string = self.title['html']
        
        if filename == self.config['info']['Services']['logname']:
            h1_name_descr = self.soup.select_one('h1#name_descr')
            h1_name_descr.string = self.NAME + ' :: ' + self.DESCR
        
            h2_about = self.soup.select_one('h2#about')
            h2_about.string = self.text['about']['historic']
            
            ul_nav = self.soup.select_one('ul#nav')
            # insert link
            ul_nav.insert(0, self.link)
            
        span_name = self.soup.select_one('span#name')
        span_name.string = self.NAME
            
        a_url_web = self.soup.select_one('a#url_web')
        a_url_web.string = self.DESCR
        a_url_web['href'] = self.url['web']
        
        if filename == self.files['html']:
            span_url_historic = self.soup.select_one('span#url_historic')
            span_url_historic.string = self.text['html']['url_historic']
            
        span_date = self.soup.select_one('span#date_copyright')
        span_date.string = self.text['about']['copyright']
            
        span_license = self.soup.select_one('span#license')
        span_license.string = self.text['about']['license']
        
        # insert article
        self.soup.body.section.insert(0, self.article)

        tool.write_file(self.soup.prettify(formatter=None), filename)

    def set_datas(self, datas):
        ''' Set needed datas informations '''
        self.datas = datas
    
    def set_image(self, image):
        ''' Set needed information image '''
        self.image = image

    def set_response(self, urls):
        ''' Set dict urls '''
        self.urls = urls

    def write_html(self):
        ''' Write HTML Files '''
        
        list_files = [ 
            self.config['info']['Services']['logname'],
            self.files['html'],
        ]
        self.log.debug('list files: %s' % list_files)
        
        for filename in list_files:
        
            if filename == self.files['html']:
                if not tool.check_files_exists(filename):
                    shutil.copy(self.files['src']['html'], filename)
            
            code = tool.read_file(filename)
            self.soup = BeautifulSoup(code, "html.parser")
            
            self.build_html(filename)
            self.modify(filename)
            
            del(code)
