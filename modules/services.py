#!/usr/bin/env python
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

#
# # Authors informations
#
# @author: HUC Stéphane
# @email: <devs@stephane-huc.net>
# @url: http://stephane-huc.net
#
# @license : OpenBSD
#

''' Manage services to retrieving A Network Resource'''

import os
import re
import shutil
import sys
import string
import urllib

# import my modules  
try:
    from modules.notification import Info as info
except ImportError:
    print(f'Cant import notification module onto Service')
    sys.exit(1)

try:
    from modules.message import Message as mssg
except ImportError:
    print(f'Cant import window_message module onto Service')
    sys.exit(1)
    
try:
    from modules.tools import Tools as tool
except ImportError:
    print(f'Cant import tools module onto Service')
    sys.exit(1)


class Services(object):
    '''Services sending data, and examining responses'''

    def __init__(self, init):

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

        self.datas = dict()
        self.html = dict()
        self.image = dict()
        self.response = ''
        self.urls = list()

        self.dict_for_info = dict()     # dict for class Info
        self.dict_for_info = {
            'icon': self.files['images']['icon'],
            'NAME': self.NAME,
            'title': self.title['main_window'],
        }

        self.dict_for_tools = dict()    # dict for class Tools
        self.dict_for_tools = {
            'config': self.config,
            'debug': self.debug,
            'DESCR': self.DESCR,
            'icon': self.files['images']['icon'],
            'files': self.files, 
            'log': self.log,
            'NAME': self.NAME,
            'text': self.text,
            'title': self.title,
            'url': self.url,
        }

        self.dict_for_mssg = dict()    # dict for class Message
        self.dict_for_mssg = {
            'debug': self.debug,
            'log': self.log,
        }

        self.info = info(self.dict_for_info)
        self.tool = tool(self.dict_for_tools)
        self.wmssg = mssg(self.dict_for_mssg)

        self.log.info('=> Using services')

    def authentification(self, datas):
        '''Attempt an authentication'''

        from requests.auth import HTTPBasicAuth
        requests.post(datas['url'], auth=(datas['user'], datas['passwd']))
        self.info.notify(request.status_code)

    def get_image(self):
        ''' Get datas image '''
        return self.image

    def get_urls(self):
        ''' Get dict urls '''
        return self.urls

    def mng_response(self):
        '''Manage response to build urls needed'''

        self.log.info('*** Service Manage Response: %s ' % self.service)
        self.log.debug('Managed URL Response :: self.response: %s' % 
            self.response)

        if self.service == 'framapic' or self.service == 'lutim':

            self.mng_response_ltm()

        elif self.service == 'debianfr' or self.service == 'toilelibre':

            self.mng_response_pix()
        
        elif self.service == 'jirafeau':
            
            self.mng_response_jrf()

        if self.urls:
            if self.debug:
                self.log.debug(
                    'Managed URL Response :: self.urls: %s' % self.urls)
            return True
            
        else:
            self.wmssg.display('error', self.text['error']['build_urls'])
            return False

    def mng_response_jrf(self):
        '''Manage Response from Jirefeau service'''
        self.log.info('*** Services Manage Response : %s' % self.service)
        
        codes = self.response.splitlines()
        self.log.debug('Split response :: codes:%s' % codes)
        
        length = len(codes)
        
        if length == 2:
            # url
            self.urls.append(
                self.actions['jirafeau']['urls']['web'] + '/f.php?h=' + \
                codes[0])
            # img
            self.urls.append(
                self.actions['jirafeau']['urls']['web'] + '/f.php?h=' + \
                codes[0] + '&p=1')
            # url to dl
            self.urls.append(
                self.actions['jirafeau']['urls']['web'] + '/f.php?h=' + \
                codes[0] + '&d=1')
        
        elif length == 3:
            # url
            self.urls.append(
                self.actions['jirafeau']['urls']['web'] + '/f.php?h=' + \
                codes[0] + '&k=' + codes[2])
            # img
            self.urls.append(
                self.actions['jirafeau']['urls']['web'] + '/f.php?h=' + \
                codes[0] + '&k=' + codes[2] + '&p=1')
            # url to dl
            self.urls.append(
                self.actions['jirafeau']['urls']['web'] + '/f.php?h=' + \
                codes[0] + '&k=' + codes[2] + '&d=1')
        
        # url to del
        self.urls.append(
            self.actions['jirafeau']['urls']['web'] + '/f.php?h=' + codes[0] + \
            '&d=' + codes[1])
        
        if length == 2:
            # for forum
            self.urls.append(
                '[url=' + self.actions['jirafeau']['urls']['web'] + \
                '/f.php?h='  + codes[0] + '&d=1][img]' + \
                self.actions['jirafeau']['urls']['web'] + '/f.php?h=' + \
                codes[0] + '&p=1[/img][/url]')
            # for site
            self.urls.append(
                '<a href="' + self.actions['jirafeau']['urls']['web'] + \
                '/f.php?h=' + codes[0] + '&d=1" title="' + \
                self.text['html']['jirafeau']['infos'][1] + \
                self.image['basename'] + '"><img alt="' + \
                self.image['basename'] + '" src="' + \
                self.actions['jirafeau']['urls']['web'] + '/f.php?h='  + \
                codes[0] + '&p=1"/></a>'
                )
            
        del(length)

        if self.urls:
            self.log.debug('urls modified: ' + str(self.urls))
            return True
        
        else:
            return False
            
    def mng_response_ltm(self):
        '''Manage Response from Lutim service or assimilated'''
        self.log.info('*** Services Manage Response : %s' % self.service)

        if self.image['format_output'] == 'json':

            if 'success' in self.response:
                
                if self.response['success'] :
                    self.log.debug(
                        'Basename Image: %s ' % self.image['basename'])
                    self.log.debug('Filename Image response: %s ' % 
                        self.response['msg']['filename'])
                
                    filename = urllib.parse.unquote(
                        self.response['msg']['filename'])
                    
                    self.log.debug('Filename decode: %s ' % filename)
                    # verify name image
                    if filename == self.image['basename']:
                        # url
                        self.urls.append(
                            self.actions[self.service]['urls']['upload'] + \
                            '/' + self.response['msg']['short'])
                        # img
                        self.urls.append(
                            self.actions[self.service]['urls']['upload'] + \
                            '/' + self.response['msg']['short'])
                        # mini for forum
                        self.urls.append('[url=' + self.urls[1] + '][img]' + \
                            self.urls[1] + '[/img][/url]')
                        # url to dl
                        self.urls.append(self.urls[1] + '?dl')
                        # url to share
                        self.urls.append(self.urls[1] + '?t')
                        # url to del
                        self.urls.append(
                            self.actions[self.service]['urls']['upload'] + \
                            '/d/' + self.response['msg']['real_short'] + \
                            '/' + self.response['msg']['token'])
                
                    else:
                        self.wmssg.display('error', 
                            self.text['notify']['img_name_not_match'])
                
                    del(filename)
                    
                else:
                    mssg = self.text['notify']['json_success_false']
                    if 'msg' in self.response['msg']:
                        mssg = mssg + self.response['msg']['msg']
                        
                    self.info.notify(mssg, 'warning')
                    self.wmssg.display('error', mssg)
                    del(mssg)
                
            else:
                self.info.notify(self.text['notify']['json_unsuccess'],
                    'warning')
                self.wmssg.display('error',
                    self.text['notify']['json_unsuccess'])

        else:

            info = list()
            reg = re.compile(r'<strong>(.*?)</strong>', re.MULTILINE)
            info.append(reg.findall(self.response))
            del(reg)

            patterns = [
                # for short information
                '<input name="image_url" type="hidden" value="' + \
                    self.actions[self.service]['urls']['upload'] + '/',
                # for real_short + token informations
                '<form class="form" role="form" method="POST" action="' + \
                    self.actions[self.service]['urls']['upload'] + '/m/',
            ]

            for pattern in patterns:
                reg = re.compile(r'' + pattern + '(.*?)">')
                string = reg.findall(self.response)
                del(reg)
                info.append(string)
            # verify name img
            if info[0][0] == self.image['basename']:
                # url
                self.urls.append(
                    self.actions[self.service]['urls']['upload'] + '/' + \
                    info[1][0])
                # img
                self.urls.append(
                    self.actions[self.service]['urls']['upload'] + '/' + \
                    info[1][0])
                # mini for forum
                self.urls.append('[url=' + self.urls[1] + '][img]' + \
                    self.urls[1] + '[/img][/url]')
                # url to dl
                self.urls.append(self.urls[1] + '?dl')
                # url to share
                self.urls.append(self.urls[1] + '?t')
                # url to del
                self.urls.append(
                    self.actions[self.service]['urls']['upload'] + '/d/' + \
                    info[2][0])

            del(info, patterns)
        
        if self.urls:
            self.log.debug('urls modified: ' + str(self.urls))
            return True
        
        else:
            return False

    def mng_response_pix(self):
        '''Manage Response from Toile Libre service or assimilated'''
        self.log.info('*** Services Manage Response : %s' % self.service)

        reg = re.compile(r'<textarea>(.*?)</textarea>',  re.MULTILINE)
        self.urls = reg.findall(self.response)
        del(reg)
        self.log.debug('urls: ' + str(self.urls))

        for key, val in enumerate(self.urls):
            self.urls[key] = val.replace("'",'"')
            self.log.debug('urls: key ' + str(key) +' :: value ' + \
                self.urls[key])

            if key == 4:
                replace = r'<a href="\1" title="' + \
                    self.text['html']['img_mini']  + \
                    self.image['basename'] + '"><img alt="' + \
                    self.image['basename'] + r'" src="\2"/></a>'

            elif key == 5:
                replace = r'<a href="\1" title="' + \
                    self.text['html']['pix']['infos'][1] + \
                    self.image['basename'] + '"><img alt="' + \
                    self.image['basename'] + r'" src="\2"/></a>'

            else:
                replace = r'<a href="\1" title="' + \
                    self.text['html']['pix']['infos'][key] + \
                    self.image['basename'] + '"><img alt="' + \
                    self.image['basename'] + r'" src="\2"/></a>'
            
            reg = re.compile(r'<a href="(.*?)"><img src="(.*?)" /></a>')
            self.urls[key] = reg.sub(replace, self.urls[key])
            del(replace, reg)

        if self.urls:
            self.log.debug('urls modified: ' + str(self.urls))
            return True
        
        else:
            return False

    def upload(self, datas):
        '''Upload image to the service'''

        self.log.info('*** Service Upload: %s' %self.service)

        import mimetypes
        import requests
        import time
        
        self.datas = datas

        #if os.path.getsize(datas['image']) == 0:
            #self.wmssg.display('error', self.text['error']['size'])
            #return False
        if 'max_file_size' in self.actions[self.service]:
            if tool.check_size(datas['image'], 
                self.actions[self.service]['max_file_size']) == False:
                    return False
        
        else:
            if tool.check_size(datas['image']) == False:
                return False

        self.image['file'] = datas['image']
        self.image['basename'] = os.path.basename(datas['image'])
        self.image['timestamp'] = int(time.time())
        self.image['time_info'] = time.strftime("%Y-%m-%d %X")
        self.image['time_human'] = time.strftime("%d-%m-%Y") + \
            self.text['html']['to'] + time.strftime("%X")

        if 'format' in datas:
            self.image['format_output'] = datas['format']

        # encode into htmlentities
        img = urllib.parse.quote(self.image['basename'])    #py3
        # mime type
        mime = mimetypes.guess_type(datas['image'])[0]

        if self.service == 'framapic' or self.service == 'lutim':

            info = {'file': (img, open(datas['image'], 'rb'), mime)}

        elif self.service == 'debianfr' or self.service == 'toilelibre':

            info = {'img': (img, open(datas['image'], 'rb'), mime)}
        
        elif self.service == 'jirafeau' or self.service == "debianfacile":
            
            info = {'file': (img, open(datas['image'], 'rb'), mime)}

        del img

        self.log.debug('Dict Info: ')
        self.log.debug(info)

        try:
            # build scheme url
            url = self.actions[self.service]['urls']['upload'].split('/')
            scheme = url[0].rstrip(':').lower()
            self.log.debug('scheme: %s' % scheme)
            del url

            # get request
            if scheme == 'https':

                request = requests.post(
                    self.actions[self.service]['urls']['upload'],
                    files=info, data=datas, verify=True)

            elif scheme == 'http':

                request = requests.post(
                    self.actions[self.service]['urls']['upload'],
                    files=info, data=datas, verify=False)

            self.log.debug(request)
            del scheme

            try:
                # manage informations segun request
                ctype = request.headers['Content-Type']
                status_code = request.status_code
                status_raise = request.raise_for_status()

                self.log.info('Status Code Request: %s' % status_code)
                self.log.debug('HTTP Error Request: %s' % status_raise)
                self.log.debug('Request Headers: %s' % ctype)

                request.encoding = 'utf-8'

                #if ctype == 'application/json':
                if 'application/json' in ctype:
                    self.response = request.json()
                else:
                    self.response = request.text

                self.log.debug('Response: %s' % self.response)
                
                request.close()
                del request

            except NameError:
                self.wmssg.display('error', self.text['error']['req_not_build'])
                return False

            del(mime, info)

            if self.debug:
                filename = os.path.join(
                    self.dir, 'log', self.service + '_response.log')
                self.log.debug('File Request: %s' % filename)

                if self.tool.write_file(str(self.response), filename):
                    self.info.notify(self.text['notify']['rep_req'])

                del filename

            return True

        except requests.exceptions.ConnectionError as rec:
            self.log.exception('Connection Error Request: %s' % rec)
            self.wmssg.display('error', self.text['error']['connection'])
            return False

        except requests.exceptions.HTTPError as reh:
            self.log.exception('HTTP Error Request: %s' % reh)
            self.wmssg.display('error', self.text['error']['http'])
            return False

        except requests.exceptions.URLRequired as reu:
            self.log.exception('URL Error Request: %s' % reu)
            self.wmssg.display('error', self.text['error']['url'])
            return False

        except requests.exceptions.TooManyRedirects as retmr:
            self.log.exception('Error Request: %s' % retmr)
            self.wmssg.display('error', self.text['error']['redirect'])
            return False

        #except requests.exceptions.ConnectTimeout as rect: # not exists !?
            #self.log.exception('TimeOut Error Request: %s' % rect)
            #self.wmssg.display(
                #gtk.MESSAGE_ERROR, self.text['error']['timeout_connect'])
            #return False

        #except requests.exceptions.ReadTimeout as rert:    # not exists !?
            #self.log.exception('TimeOut Error Request: %s' % rert)
            #self.wmssg.display(
                #gtk.MESSAGE_ERROR, self.text['error']['timeout_read'])
            #return False

        except requests.exceptions.Timeout as ret:
            self.log.exception('TimeOut Error Request: %s' % ret)
            self.wmssg.display('error', self.text['error']['timeout'])
            return False
