#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

#
# # Authors informations
#
# @author: HUC Stéphane
# @email: <devs@stephane-huc.net>
# @url: http://stephane-huc.net
#
# @license : BSD "Simplified" 2 clauses
#

''' Message is a module to display-it by gtk, pygtk '''

import sys
import tkinter as tk
import tkinter.messagebox as mbox

class Message(object):
    '''Display a window message'''

    def __init__(self, init):

        for i in init:
            setattr(self, i, init[i])

    def display(self, flag=None, mssg=None):
        '''Displaying Window Message'''

        self.log.info('=> Displaying Window Message')
        self.log.debug('Flag: ' + str(flag))

        if flag == 'error':
            mbox.showerror(message=mssg)
        elif flag == 'info':
            mbox.showinfo(message=mssg)

        self.log.info('=> Destroying Window Message')
