#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

#
# # Authors informations
#
# @author: HUC Stéphane
# @email: <devs@stephane-huc.net>
# @url: http://stephane-huc.net
#
# @license : BSD "Simplified" 2 clauses
#

''' Manage notifications systems '''

import notify2

class Info(object):
    '''notifications'''

    def __init__(self, init):

        # building self
        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

    def notify(self, text, urgency=None):
        '''Notify systems'''

        if urgency is None:
            urgency = notify2.URGENCY_NORMAL
        elif urgency == 'warning':
            urgency = notify2.URGENCY_CRITICAL

        notify2.init(self.NAME)
        notification = notify2.Notification(
            self.title, text, self.icon)
        notification.set_timeout(notify2.EXPIRES_DEFAULT)
        notification.set_urgency(urgency)
        notification.show()
