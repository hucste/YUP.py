#!/usr/bin/env python3

#
# # Authors informations
#
# @author: HUC Stéphane
# @email: <devs@stephane-huc.net>
# @url: http://stephane-huc.net
#
# @license : OpenBSD
#

'''Class Worker for YUP application'''

import time
import threading

# Import Personals Modules
try:
    from modules.html import ToolsHTML
except ImportError:
    print(f'Cant import html module onto Service')
    sys.exit(1)
    
try:
    from modules.services import Services as Serv
except ImportError:
    print(f'Cant import services module onto Gui')
    sys.exit(1)

try:
    from modules.tools import Tools
except ImportError:
    print(f'Cant import tools module onto Gui')
    sys.exit(1)


class Worker(threading.Thread):
    
    def __init__(self, gui):
        super().__init__()
        
        self.bits = 0
        self.datas = {}
        self.init = {}
        self.size = 0
        self.urls = list()
        
        self.gui = gui
    
    def HTMLWorker(self):
        ''' Call Tools to threat HTML '''
        htool = ToolsHTML(self.init)
        htool.set_datas(self.datas)
        htool.set_image(self.image)
        htool.set_response(self.urls)
        htool.write_html()
    
    def display_historic(self, log):
        ''' Display historic '''
        tool = Tools(self.init)
        tool.open_historic(log)
    
    def mng_cursor(self):
        ''' Manage cursor '''
        self.cursor = self.gui['cursor']
        self.gui['cursor'] = 'watch'
        self.gui.update()

    def restore_cursor(self):
        ''' Restore cursor '''
        self.gui['cursor'] = self.cursor

    def send_hosting(self):
        '''Send datas to hosting images'''
        serv = Serv(self.init)
        if serv.upload(self.datas):

            if serv.mng_response():
                self.image = serv.get_image()
                self.urls = serv.get_urls()
                return True
                
        else:
            return False

    def set_datas(self, datas):
        ''' Set datas '''
        self.datas = datas
    
    def set_init(self, init):
        ''' Set init '''
        self.init = init
        
    def set_size(self, size):
        ''' Define size of image '''
        self.size = size
