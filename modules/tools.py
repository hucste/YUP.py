#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

#
# # Authors informations
#
# @author: HUC Stéphane
# @email: <devs@stephane-huc.net>
# @url: http://stephane-huc.net
#
# @license : OpenBSD
#

''' Tools needed '''

from bs4 import BeautifulSoup
import os
import pprint
import re
import webbrowser as web

# Import Personals Modules
try:
    from modules.notification import Info as info
except ImportError:
    print(f'Cant import notification module onto Service')
    sys.exit(1)

try:
    from modules.message import Message as mssg
except ImportError:
    print(f'Cant import window_message module onto Service')
    sys.exit(1)


class Tools(object):
    '''Tools for YUP'''

    def __init__(self, init):

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

        self.ppp = pprint.PrettyPrinter(indent=4)
        
        self.dict_for_info = dict()     # dict for class Info
        self.dict_for_info = {
            'icon': self.icon,
            'NAME': self.NAME,
            'title': self.title['main_window'],
        }

        self.dict_for_mssg = dict()    # dict for class Message
        self.dict_for_mssg = {
            #'debug': self.debug,
            'log': self.log,
        }

        self.info = info(self.dict_for_info)
        self.wmssg = mssg(self.dict_for_mssg)

        self.log.info('=> Using Tools ...')

    @staticmethod
    def array_search(haystack, needle):
        ''' Equivalent PHP array_search() '''

        return [k for k, v in enumerate(haystack) if v == needle][0]

    def array_search_by_label(self, haystack, needle):
        ''' Search k if label exists and comp with needle '''

        try:
            return [
                k for k, v in enumerate(haystack['index'])
                if self.cmp(needle, haystack[v]['label']) == 0][0]

        except IndexError as iee:
            print(f'Error Index => {iee}')
            self.ppp.pprint(haystack)
            print(f'needle: {needle}')
            self.log.debug('haystack: \n' + str(self.ppp.pformat(haystack)))
            self.log.debug('needle: %s ' % needle)

    def array_search_by_label2(self, haystack, needle):
        ''' Search v if label exists and comp with needle '''

        try:
            return [
                v for k, v in enumerate(haystack['index'])
                if self.cmp(needle, haystack[v]['label']) == 0][0]

        except IndexError as iee:
            print(f'Error Index => {iee}')
            self.ppp.pprint(haystack)
            print(f'needle: {needle}')
            self.log.debug('haystack: \n' + str(self.ppp.pformat(haystack)))
            self.log.debug('needle: %s ' % needle)

    @staticmethod
    def check_files_exists(file_cfg):
        '''Check if config files exists'''

        try:
            with open(file_cfg) as file:
                return True

        except IOError as ioe:
            print(f'Error to read document: {ioe}')
            return False

    def cmp(self, value, needle):
        '''Compare two values'''
        return (value>needle)-(value<needle)

    def fill_tk_cbb(self, haystack):
        '''Fill dynamically combobox'''
        self.log.debug('Haystack: %s' % haystack)

        array = list()
        for key, val in enumerate(haystack['index']):
            if haystack[val]['active']:
                array.append(haystack[val]['label'])

        if array:
            return array
        del array

    @staticmethod
    def check_size(needle, auth=0):
        '''Compare size file with max authorized'''
        
        size = Tools.get_size(needle)
        
        if size == 0:
            self.wmssg.display('error', self.text['error']['size'])
            return False
        
        elif auth > 0 and size > auth:
            self.wmssg.display('error', self.text['error']['big_size'])
            return False
        
        else:
            return True
    
    @staticmethod
    def get_size(needle):
        '''Get file size'''
        return os.path.getsize(needle)

    @staticmethod
    def in_array(haystack, needle):
        ''' Equivalent PHP in_array() '''

        if needle in haystack:
            return True
        else:
            return False

    def open_historic(self, log):
        '''Display historic file html'''
        web.open_new_tab('file://' + log)
        self.log.debug('log: %s ' %log)

    @staticmethod
    def read_file(filename):
        '''Read file'''

        try:
            with open(filename, 'r') as rfile:
                strings = rfile.read()
                rfile.close()
                
                return strings

        except IOError as ioe:
            self.log.exception('Error to read document: %s' % ioe)
            self.wmssg.display('error', self.texts['error']['read_file'] % ioe)
            return False

    @staticmethod
    def rebuild_dict(haystack):
        ''' Rebuild dictionnary to remove index if active is false '''

        haystack['key'] = haystack['index']
        haystack['index'] = list()

        for name in haystack['key']:

            if haystack[name]['active']:
                haystack['index'].append(name)

        del haystack['key']
        return haystack

    def sed_lines(self, filename, pattern=None, replace=None):
        '''Edit file to replace lines'''

        try:
            self.log.info('=> Sed Lines :: Filename : %s' % filename)
            self.log.debug('=> Sed Lines :: pattern : %s' % pattern)

            with open(filename, 'r') as rfile:
                lines = rfile.readlines()
                self.log.debug('=> Sed Lines :: Lines: %s' % lines)

            with open(filename, 'w') as wfile:
                for line in lines:

                    if pattern in line:
                        reg = re.compile(r'' + pattern, re.MULTILINE)
                        line = reg.sub(replace, line)
                        del(reg)

                    wfile.write(line)
                            
            return True

        except ImportError as iee:
            self.log.exception('Error to import module: %s' % iee)
            return False

        except IOError as ioe:
            self.log.exception('Error to write document: %s' % ioe)
            self.wmssg.display('error', self.texts['error']['write_file'] % ioe)
            return False

    def sed_tags(self, filename):
        '''Edit file html historic to replace tags'''

        try:
            patterns = [
                '{ABOUT}',
                '{COPYRIGHT}',
                '{DESCR}',
                '{LANG}',
                '{LICENCE}',
                '{NAME}',
                '{TITLE}',
                '{URL_HISTORIC}',
                '{URL_WEB}',
            ]
            replaces = [
                self.text['about']['historic'],
                self.text['about']['copyright'],
                self.DESCR, 
                self.config['lang'], 
                self.text['about']['license'],
                self.NAME, 
                self.title['html'],
                self.text['html']['url_historic'],
                self.url['web'],
            ]

            with open(filename, 'r') as rfile:
                lines = rfile.readlines()
                self.log.debug('=> Sed Lines :: Lines: %s' % lines)

            with open(filename, 'w') as wfile:
                for line in lines:

                    for key, pattern in enumerate(patterns):
                        if pattern in line:
                            reg = re.compile(r'' + pattern, re.MULTILINE)
                            line = reg.sub(replaces[key], line)
                            del(reg)

                    wfile.write(line)

            return True

        except ImportError as iee:
            self.log.exception('Error to import module: %s' % iee)
            return False

        except IOError as ioe:
            self.log.exception('Error to write document: %s' % ioe)
            self.wmssg.display('error', self.texts['error']['write_file'] % ioe)
            return False

    @staticmethod
    def write_file(text, filename):
        ''' Write file '''

        try:
            with open(filename, 'w') as wfile:
                wfile.write(text)
                wfile.close()

                return True

        except IOError as ioe:
            self.log.exception('Error to write document: %s' % ioe)
            self.wmssg.display('error', self.texts['error']['write_file'] % ioe)
            return False
