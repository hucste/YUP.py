#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-

#
# # Authors informations
#
# @author: HUC Stéphane
# @email: <devs@stephane-huc.net>
# @url: http://stephane-huc.net
#
# @license : OpenBSD
#

''' Using config files '''

from configparser import ConfigParser
from configparser import SafeConfigParser

import os
import pprint
import shutil
import sys

# Import Personals Modules
try:
    from modules.notification import Info as info
except ImportError:
    print(f'Cant import notification module onto Config')
    sys.exit(1)

try:
    from modules.tools import Tools as tool
except ImportError:
    print(f'Cant import tools module onto Config')
    sys.exit(1)


class Config(object):
    '''Manage Config'''

    def __init__(self, init):

        # building self
        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

        self.ppp = pprint.PrettyPrinter(indent=4)

        dict_for_info = dict()     # dict for class Info
        dict_for_info = {
            'icon': self.icon,
            'NAME': self.NAME,
            'title': self.title['main_window'],
        }
        dict_for_tools = dict()    # dict for class Tools
        dict_for_tools = {
            'config': self.config,
            #'debug': self.debug,
            'DESCR': self.DESCR,
            'icon': self.icon,
            'log': self.log,
            'NAME': self.NAME,
            'text': self.text,
            'title': self.title,
            'url': self.url,
        }

        self.info = info(dict_for_info)
        self.tool = tool(dict_for_tools)

        self.log.info('=> Using Tools ...')

    def build_personals_files(self):
        '''Build personals necessaries files'''

        # mkdir personal config Yup if not exists
        if not os.path.isdir(self.files['src']['personal']):
            os.makedirs(self.files['src']['personal'])
        
        # copy personal config Yup, and modify it
        if not self.tool.check_files_exists(self.files['config']):
            shutil.copy(
                self.files['src']['config'], self.files['src']['personal'])

            self.write_config(self.files['config'])
        
        # copy historic template file html, and fill it
        if not self.tool.check_files_exists(
            self.config['info']['Services']['logname']):
            shutil.copy(
                self.files['src']['historic'], self.files['src']['personal'])

            self.tool.sed_tags(self.config['info']['Services']['logname'])
        
        # copy boostrap files
        for value in self.files['src']['bootstrap'].values():
            if self.tool.check_files_exists(value):
                shutil.copy(value, self.files['src']['personal'])

    def get_config(self, file_config):
        '''Get infos from file config'''

        from io import StringIO

        haystack = dict()
        section = 'Main'

        parser = ConfigParser()
        with open(file_config, 'r') as stream:
            fakefile = StringIO("["+section+"]\n" + stream.read())
            parser.readfp(fakefile)

        options = parser.options(section)
        for option in options:
            val = parser.get(section, option)

            if option == 'mini_width':
                haystack[option] = parser.getint(section, option)
            elif option == 'service_defaut':
                for string in ['"', '-', '_', ' ']:
                    val = val.replace(string, '')

                haystack[option] = val.lower()
            else:
                if val == 'true':
                    haystack[option] = True
                elif val == 'false':
                    haystack[option] = False
                else:
                    haystack[option] = val.strip('"')

        return haystack

    def get_config2(self, file_config):
        '''Get infos from config file'''

        haystack = dict()

        parser = SafeConfigParser()
        parser.read(file_config)

        for section in parser.sections():
            haystack[section] = dict()

            for name, value in parser.items(section):
                if name == 'mini_width':
                    haystack[section][name] = int(value)
                elif value == 'True' or value == 'False':
                    haystack[section][name] = parser.getboolean(section, name)
                else:
                    haystack[section][name] = str(value)

        return haystack

    def write_config(self, file_config):
        '''Write config file'''

        self.log.info('File config: %s' % file_config)
        self.log.debug('Config Info: %s' % self.config['info'])

        parser = SafeConfigParser()

        for section in self.config['info']:
            parser.add_section(section)

            for key, value in enumerate(self.config['info'][section]):

                info = str(self.config['info'][section][value])

                parser.set(section, value, info)

        try:
            parser.write(open(file_config, 'w'))
            self.info.notify(self.text['notify']['cfg_saved'])

        except IOError:
            self.info.notify(self.text['notify']['cfg_unsaved'], 'warning')

        except:
            self.info.notify(self.text['notify']['cfg_unsaved'])

