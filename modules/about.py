#!/usr/bin/env python3

#
# # Authors informations
#
# @author: HUC Stéphane
# @email: <devs@stephane-huc.net>
# @url: http://stephane-huc.net
#
# @license : OpenBSD
#

'''Build Self Window About'''

import tkinter as tk
import tkinter.ttk as ttk

import PIL.Image as pimg
import PIL.ImageTk as pimgtk

class gui(tk.Tk):
    ''' Display a GUI for window about'''

    def __init__(self, parent, init):
        tk.Toplevel.__init__(self, parent)
        self.parent = parent

        # initialise self
        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

        self.initialize()
    
    def initialize(self):
        '''Initialize window'''
        self.grid()

        self.display_icon()
        self.ui()

        self.update()
        self.geometry(self.geometry())

    def display_icon(self):
        '''Manage icon'''
        self.call('wm', 'iconphoto', self, self.icon)

    def on_btn_close_clicked(self):
        '''  '''
        self.quit()
        self.destroy()

    def ui(self):
        '''Displaying UI'''
        # Frame for service informations
        frm_service = ttk.Frame(self)
        frm_service.grid(row=7, column=1)

        ## Add an image logo
        img_service = ttk.Label(frm_service, image=self.logo)
        img_service.grid(row=0, column=0, rowspan=2)

        lbl_name = tk.Label(frm_service, anchor=tk.W, font=('', 20, 'bold'),
                            justify=tk.LEFT, padx=20, pady=20,
                            text=self.NAME)
        lbl_name.grid(row=0, column=1)

        lbl_descr = tk.Label(frm_service, font=('', 16, ''), padx=15, pady=15,
                             text=self.DESCR)
        lbl_descr.grid(row=1, column=1)

        lbl_author = tk.Label(frm_service, padx=10, pady=10,
                              text=self.texts['about']['copyright'])
        lbl_author.grid(row=3, column=0, columnspan=2)

        lbl_comments = tk.Label(frm_service, justify=tk.LEFT, padx=7, pady=7,
                                text=self.texts['about']['comments'])
        lbl_comments.grid(row=4, column=0, columnspan=2)

        lbl_web = tk.Label(frm_service, justify=tk.LEFT, padx=7, pady=7,
                           text=self.urls['web'])
        lbl_web.grid(row=5, column=0, columnspan=2)

        btn_close = ttk.Button(frm_service, style="Green.TButton",
                               text=self.texts['label']['btn_close'],
                               command=self.on_btn_close_clicked)
        btn_close.grid(row=6, column=0, columnspan=2)
