YUP.py :: Yet another Uploader Pixxie
==============================

*(Version Python 3.x)*

Script Python *(tkinter)* pour téléverser des images vers des sites  d'hébergements pour insertion dans forum.

Est capable d'utiliser les services de :

- basé sur pix : [Toile Libre](https://pix.toile-libre.org), *~~[Debian-fr](https://pix.debian-fr.xyz)~~ (service arrêté)*
- basé sur lut.im : [Framapic](https://framapic.org) *~~[Lutim](https://lut.im)~~ (service arrêté)*
- [Jirafeau](https://jirafeau.net)

Auteurs
-------

**HUC Stéphane** : alias "PengouinPdt"

 - **Mail** : *<devs@stephane-huc.net>*
 - **GIT** : *https://framagit.org/hucste/YUP.py* 
 - **GPG:Fingerprint** : *CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58* 

**Licence** : *BSD Simplifiée*

----

**Dépendances** :

- Debian Sid: 
    - apt install python3 python3-tk python-beautifulsoup python3-pydbus python3-pil python3-pil.imagegtk python3-pip python3-requests python3-notify2
- OpenBSD 6.4 : 
    - pkg_add python-3.6.6p1 python-tkinter-3.6.6p1
- OpenBSD 6.3 :  
    - pkg_add python-3.6.2 python-tkinter-3.6.2 
- OpenBSD (supplémentaires) : 
    - pkg_add py3-Pillow py3-beautifulsoup4 py3-dbus py3-pip py3-requests
    - python3.6 -m pip install --upgrade configparser notify2

----

UTILISATION
-----------

chmod 0700 yup.py <br />
./yup.py
